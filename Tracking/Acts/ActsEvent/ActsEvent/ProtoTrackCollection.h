/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef ActsEvent_PROTOTRACKCOLLECTION_H
#define ActsEvent_PROTOTRACKCOLLECTION_H

#include "AthenaKernel/CLASS_DEF.h"
#include "ActsEvent/ProtoTrack.h"

namespace ActsTrk {
    typedef std::vector<ActsTrk::ProtoTrack> ProtoTrackCollection;
};
CLASS_DEF( ActsTrk::ProtoTrackCollection , 1189038362 , 1 )



#endif 
