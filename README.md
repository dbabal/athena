ATLAS Offline software
========================

Welcome to [ATLAS](https://atlas.ch) Athena software! 

Please visit the [public documentation](https://atlassoftwaredocs.web.cern.ch) for this project. To contribute or to read about our development workflow, follow the [developers' instructions](https://atlassoftwaredocs.web.cern.ch/athena/developers/). 

For ATLAS members, please see the [Atlas Computing Twiki](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/AtlasComputing) for even more information.

Branches
--------
The following major branches are currently active:

Branch                                                            | Purpose                      | Main Project, Release Series
------------------------------------------------------------------| ---------------------------- | ---------------------------------------
[main](https://gitlab.cern.ch/atlas/athena/tree/main)             | Upgrade, Analysis, Derivations | Athena 25.0.X, (Ath)AnalysisBase 25.2.X
[24.0](https://gitlab.cern.ch/atlas/athena/tree/24.0)             | Run-3 Tier0, Point1, MCProd  | Athena 24.0.X, AthSimulation 24.0.X
[21.2](https://gitlab.cern.ch/atlas/athena/tree/21.2)             | Legacy run 2 derivations     | AthDerivation 21.2.X

Links
-----

- The [ATLAS webpage](https://atlas.ch) will tell you all about the ATLAS experiment (for ATLAS members, the collaboration webpage is [here](https://atlas-collaboration.web.cern.ch/))
- The [software documentation](https://atlassoftwaredocs.web.cern.ch) page is the main public entry point for this project
- The [Twiki](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/AtlasComputing) is the main source of internal documentation, and has many links to sub-domains
- The git repository is [here](https://gitlab.cern.ch/atlas/athena)
- The [ATLAS Nightlies and CI page](https://bigpanda.cern.ch/globalview/) shows the build status of all nightlies
- The [Doxygen code documentation](https://atlas-sw-doxygen.web.cern.ch/atlas-sw-doxygen/atlas_main--Doxygen/docs/html/index.html) is available for the `main` branch
- To cite Athena, you can use the following zenodo [DOI](https://zenodo.org/record/2641997)
